cc = meson.get_compiler('c')


libportability_src = []


check_functions = [
	['memrchr', 'memrchr.c', 'NEED_MEMRCHR', 'string.h'],
	['strlcpy', 'strlcpy.c', 'NEED_STRLCPY', 'string.h'],
	['pipe2', 'pipe2.c', 'NEED_PIPE2', 'unistd.h'],
	['mknodat', 'mknodat.c', 'NEED_MKNODAT', 'sys/stat.h'],
	['qsort_r', 'qsort_r.c', 'NEED_QSORT_R', 'stdlib.h'],
]


foreach f : check_functions
	if not cc.has_function(f.get(0), prefix: '#include <' + f.get(3) + '>', args: ['-D_GNU_SOURCE']) or not cc.has_header_symbol(f.get(3), f.get(0), args: ['-D_GNU_SOURCE'])
		add_project_arguments('-D' + f.get(2), language: 'c')
		libportability_src += [f.get(1)]
	endif
endforeach


# Check for wrong (non-POSIX) qsort_r prototype
qsort_r_test = '''
	#define _GNU_SOURCE
	#include <stdlib.h>
	_Static_assert(_Generic((qsort_r),
		void (*)(void *, size_t, size_t, void *,
			int (*)(void *, const void *, const void *)) : 1, default: 0),
		"Bad prototype not matched");
'''
if cc.compiles(qsort_r_test, name: 'Test qsort_r non-POSIX prototype')
	add_project_arguments('-DHAVE_BROKEN_QSORT_R', language: 'c')
endif


if libportability_src.length() > 0
	libportability = static_library(
		'portability',
		libportability_src,
	)

	libportability_dep = declare_dependency(
		link_whole: libportability,
		include_directories: include_directories('.'),
	)
else
	libportability_dep = declare_dependency(
		include_directories: include_directories('.'),
	)
endif
